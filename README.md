# lvl001_ghost_theme

## Installation
```
sudo npm install -g ghost-cli@latest  
sudo npm install -g node-sass  
mkdir ghost && cd ghost  
ghost install local  
ghost start --development  
cd content/themes/  
git clone https://gitlab.com/emakitalo/lvl001_ghost_theme  
cd lvl001_ghost_theme  
node-sass --watch scss/main.scss:assets/css/style.css  
zip -r lvl001.zip lvl001_ghost_theme  
```